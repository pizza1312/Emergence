# Pylos heuristics

A project comparing heuristic rulesets for a MinMax algorithm to play the boardgame [Pylos](https://en.wikipedia.org/wiki/Pylos_(board_game)).

Went towards my first proper research paper. Was also called "Emergence" in the project files for some reason I can't remember.
